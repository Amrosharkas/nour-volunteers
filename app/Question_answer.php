<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_answer extends Model
{
    protected $table = "question_answers";
    public function getQuestion(){
        return $this->hasOne('App\Question' ,'id','question_id');
    }

    public function getEntry(){
        return $this->hasOne('App\Quiz_entry' ,'id','entry_id');
    }


}
