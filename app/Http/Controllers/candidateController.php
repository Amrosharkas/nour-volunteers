<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use App\User_grade;
use Hash;
use Mail;
use App\Question_answer;


class candidateController extends Controller
{
    public function index($user_phase,$user_status) {
    	$candidates = User::where('user_phase',$user_phase)->where('user_status',$user_status)->where('user_type','Volunteer')->get();
    	if($user_status == "Rejected"){
    		$candidates = User::where('user_status',$user_status)->where('user_type','Volunteer')->get();
    	}
        if($user_status == "all"){
            $candidates = User::where('user_type','Volunteer')->get();
        }
        $data = [];
        $data['partialView'] = 'candidates.list';
        $data['candidates'] = $candidates;
        $data['user_phase'] = $user_phase;
        $data['user_status'] = $user_status;
        return view('candidates.base', $data);
    }

    public function pass($id,Request $request){
    	$candidate = User::find($id);
    	if($candidate->user_phase == "pilotTest"){
    		$candidate->user_phase = "passedAll";
    		$candidate->user_status = "Approved";
            $subject = "Pilot Test Result";
    	}
    	if($candidate->user_phase == "phoneInterview"){
    		$candidate->user_phase = "pilotTest";
            $subject = "Phone Interview Result";
    	}
    	if($candidate->user_phase == "testEvaluation"){
    		$candidate->user_phase = "phoneInterview";
            $subject = "Islamic Knowledge Test Result";
    	}
    	$data = $request->input();
    	$grades = User_grade::where('user_id',$id)->first();
    	if($data['gradesFor'] == 'phoneInterview'){
    		$array = json_decode($data['answers'], true);
	    	$grades->fluency = $array[0];
	    	$grades->vocab = $array[1];
	    	$grades->grammar = $array[2];
	    	$grades->accent = $array[3];
    	}
    	if($data['gradesFor'] == 'pilotTest'){
    		
	    	
	    	$grades->pilot = $data['grade'];
	    	
    	}
    	$grades->save();

    	$candidate->save();
        Mail::send('emails.candidate_notification', ['candidate' => $candidate  ], function ($m) use ($candidate,$subject) {
            $m->from('platform@nouracademy.com', 'Nour Academy');
            $m->to($candidate->email, $candidate->name)->subject(' Volunteers | '.$subject);
        });

    }

    public function fail($id,Request $request){
    	$candidate = User::find($id);
    	$candidate->user_status = "Rejected";

    	$data = $request->input();
    	$grades = User_grade::where('user_id',$id)->first();
        if($candidate->user_phase == "testEvaluation"){
            $subject = "Islamic Knowledge Test Result";
        }
        if($candidate->user_phase == "phoneInterview"){
            $subject = "Phone Interview Result";
        }
        if($candidate->user_phase == "pilotTest"){
            $subject = "Pilot Test Result";
        }
    	if($data['gradesFor'] == 'phoneInterview'){
    		$array = json_decode($data['answers'], true);
	    	$grades->fluency = $array[0];
	    	$grades->vocab = $array[1];
	    	$grades->grammar = $array[2];
	    	$grades->accent = $array[3];
            $subject = "Phone Interview Result";
    	}
    	if($data['gradesFor'] == 'pilotTest'){
    		
	    	$subject = "Pilot Test Result";
	    	$grades->pilot = $data['grade'];
	    	
    	}
    	$grades->save();
    	$candidate->save();
        Mail::send('emails.failed', ['candidate' => $candidate  ], function ($m) use ($candidate,$subject) {
            $m->from('platform@nouracademy.com', 'Nour Academy');

            $m->to($candidate->email, $candidate->name)->subject(' Nour Academy | '.$subject);
            
        });
    }

    public function pilotReady(){
    	$candidate = Auth::user();
    	$candidate->pilot_ready = 1;
    	$candidate->save();
        $emails = ['mtarek@arabiclocalizer.com', 'm.abdelgawad@nouracademy.com','h.hafez@nouracademy.com'];
        Mail::send('emails.admin_notification', ['candidate' => $candidate  ], function ($m) use ($candidate,$emails) {
            $m->from('platform@nouracademy.com', 'Nour Academy');
            $m->to($emails)->subject(' Volunteers | Candidate ready for pilot test');
        });
    }

    public function add(){
        $data = [];
        $data['partialView'] = 'candidates.form';
        
        $data['user_phase'] = "";
        $data['user_status'] = "";
        return view('candidates.base', $data);
    }

    public function edit($id){
        $candidate = User::find($id);
        $data = [];
        $data['partialView'] = 'candidates.edit_form';
        $data['candidate'] = $candidate;
        $data['user_phase'] = "";
        $data['user_status'] = "";
        return view('candidates.base', $data);
    }

    public function save(Request $request){
        $data = $request->input();
        $candidate = new User();
        $candidate->name = $data['name'];
        $candidate->email = $data['email'];
        $candidate->phone = $data['mobile'];
        $candidate->user_type = "Volunteer";
        $candidate->dec_password = $data['password'];
        $candidate->password = Hash::make($data['password']);
        $candidate->save();
        Mail::send('emails.welcome', ['candidate' => $candidate  ], function ($m) use ($candidate) {
            $m->from('platform@nouracademy.com', 'Nour Academy');

            $m->to($candidate->email, $candidate->name)->subject(' Nour Academy | Welcome to our volunteering program');
            
        });
    }
    public function saveEdit(Request $request){
        $data = $request->input();
        $candidate = User::find($data['id']);
        $candidate->name = $data['name'];
        $candidate->email = $data['email'];
        $candidate->phone = $data['mobile'];
        $candidate->user_phase = $data['user_phase'];
        $candidate->user_status = $data['user_status'];
        
        $candidate->save();

        $grades = User_grade::where('user_id',$candidate->id)->first();
        $grades->quiz = $data['quiz'];
        $grades->accent = $data['accent'];
        $grades->grammar = $data['grammar'];
        $grades->pilot = $data['pilot'];
        $grades->vocab = $data['vocab'];
        $grades->fluency = $data['fluency'];
        $grades->save();
    }
    public function testDetails($candidate_id){
        $answers = Question_answer::where('user_id',$candidate_id)->get();
        $data = [];
        $data['partialView'] = 'candidates.test_details';
        $data['answers'] = $answers;
        
        $data['user_phase'] = "";
        $data['user_status'] = "";
        return view('candidates.base', $data);
    }
}
