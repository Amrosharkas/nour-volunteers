<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Quiz_entry;
use App\Question_answer;
use App\quiz;
use App\Question;
use App\Answer;
use App\User_grade;
use Mail;

class volunteersTestController extends Controller
{
	protected $logged_user;
	public function __construct(){
		$this->logged_user = Auth::user();

	}
    public function index() {
    	$data = [];
    	if($this->logged_user->quiz_status == 'NoQuiz'){
			$data['partialView'] = 'user_test.test_intro';    		
    	}
    	if($this->logged_user->quiz_status == 'Started'){
    		$next_question = $this->getNextQuestion($this->logged_user->id);
            $total_questions = Question::count();
            $data['total'] = $total_questions;
    		$data['partialView'] = 'user_test.question';
    		$data['question'] = $this->getNextQuestion($this->logged_user->id);

    		$data['entry'] = Quiz_entry::where('user_id',$this->logged_user->id)->first();
    	}
    	if($this->logged_user->quiz_status == 'Finished'){

            if($this->logged_user->user_status == "Pending"){
                if($this->logged_user->user_phase == "testEvaluation"){
                    $data['message'] = "<p>You have currently completed the Islamic knowledge test. Someone from our team will review your test and you will receive an email with the test results.   </p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
                }else{
                    $data['message'] = "<p>Congratulations you have passed the Islamic knowledge test. </p><p>In the next phase a member of our team will contact you via phone to conduct a quick interview to assess your level of English fluency. </p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;

                }
            }

            if($this->logged_user->user_status == "Rejected"){
                $data['message'] = "Unfortunately you were unable to become an Islamic Practices instructor at Nour Academy since you did not pass the Islamic Knowledge Test";
            }
    		$data['partialView'] = 'user_test.test_finished';

    	}


    	
        
        $data['user_id'] = $this->logged_user->id;
        
        $data['quizzes'] = quiz::orderBy('created_at', 'desc')->get();
        return view('user_test.base', $data);
    }

    public function startTest($user_id){
    	$check_exist = Quiz_entry::where('user_id',$user_id)->count();
    	// If no previous entry exists
    	if($check_exist == 0){
    		//Create new Entry for this user
    		$quiz_entry = new Quiz_entry();
    		$quiz_entry->user_id = $user_id;
    		$quiz_entry->quiz_id = 1;
    		$quiz_entry->start_time = date("Y-m-d H:i:s");
    		$quiz_entry->save();
    		// Get first question
    		$question = Question::orderBy('question_order','ASC')->first();

    		//Update user table
    		$user = $this->logged_user;
    		$user->quiz_status = "Started";
    		$user->quiz_start_time = $quiz_entry->start_time;
    		$user->save();

            //Create new user grades record (one to one)
            $grades = new User_grade();
            $grades->user_id = $user_id;
            $grades->save();


    	}
    	
    	$data = [];
        $total_questions = Question::count();
            $data['total'] = $total_questions;
        $data['user_id'] = $this->logged_user->id;
        $data['partialView'] = 'user_test.question';
        $data['question'] = $this->getNextQuestion($user_id);
        $data['entry'] = Quiz_entry::where('user_id',$this->logged_user->id)->first();
        return view('user_test.base', $data);

    	
    }

    public function getNextQuestion($user_id){
    	$user_id = $user_id;
    	$last_answered_question = Question_answer::where('user_id',$user_id)->orderBy('id','DESC')->first();
    	if(!$last_answered_question){
    		$last_question_order = -1;
    	}else{
    		$last_question_order = $last_answered_question->getQuestion->question_order;	
    	}
    	$next_question = Question::where('question_order' ,'>',$last_question_order)->orderBy('question_order','ASC')->first();
    	return $next_question;

    }

    public function answerQuestion(Request $request){
    	$data = $request->input();

    	$entry = Quiz_entry::where('user_id',$this->logged_user->id)->first();
    	$question = Question::find($data['question_id']);
    	$all_answers = Answer::where('question_id',$question->id)->orderBy('correct','ASC')->get();
    	$count = $all_answers->count();
    	$correct_degree = $question->weight / $count;
    	$total_degree = 0;
    	$answers = $data['answers'];

    	if($question->question_type == "Radio"){
    		$check_correct = Answer::where('question_id',$question->id)->where('id',$data['answers'])->where('correct',1)->count();
    		
    		if($check_correct == 1){

    			$total_degree = $total_degree + $question->weight;
    		}
    	}else{
    		foreach($all_answers as $answer){
    			if (in_array($answer->id, $answers) && $answer->correct == 1) {
				    $total_degree = $total_degree + $correct_degree;
				}
				if (!in_array($answer->id, $answers) && $answer->correct == 0) {
				    $total_degree = $total_degree + $correct_degree;
				}
    		}
    	}

    	$question_answer = new Question_answer();
    	$question_answer->entry_id = $entry->id;
    	$question_answer->user_id = $this->logged_user->id;
    	$question_answer->question_id = $question->id;
    	$question_answer->quiz_id = 1;
    	$question_answer->choices_ids = "";
    	$question_answer->degree = $total_degree;
    	$question_answer->save();

    	$next_question = $this->getNextQuestion($this->logged_user->id);

    	if(!$next_question){
    		$this->endTest();
    	}
    }

    public function endTest(){
        $pass_degree = 20;
        $fail_degree = 17;
    	$user_id = $this->logged_user->id;
    	$grade = Question_answer::where('user_id',$user_id)->sum('degree');
    	$entry = Quiz_entry::where('user_id',$user_id)->first();
    	$entry->finished = 1;
    	$entry->grade = $grade;
    	$entry->end_time = date("Y-m-d H:i:s");
    	$entry->save();

        $grades = User_grade::where('user_id',$this->logged_user->id)->first();
        $grades->quiz = $entry->grade;
        $grades->save();

    	$user = $this->logged_user;
    	$user->quiz_status = "Finished";
		$user->quiz_end_time = $entry->end_time;
        $candidate = $user;
        if($grade < $fail_degree){
            $user->user_phase = "testEvaluation";
            $user->user_status = "Rejected";
            $message = "Unfortunately you were unable to become an Islamic Practices instructor at Nour Academy since you did not pass the Islamic Knowledge Test";
            
            Mail::send('emails.failed', ['candidate' => $candidate  ], function ($m) use ($candidate) {
            $m->from('platform@nouracademy.com', 'Nour Academy');

            $m->to($candidate->email, $candidate->name)->subject(' Nour Academy | Islamic Knowledge Test Result');
            
            });
        }
        if($grade >= $fail_degree && $grade < $pass_degree){
            $user->user_phase = "testEvaluation";
            $user->user_status = "Pending";
            $message = "<p>You have currently completed the Islamic knowledge test. Someone from our team will review your test and you will receive an email with the test results.   </p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
            $emails = ['mtarek@arabiclocalizer.com', 'm.abdelgawad@nouracademy.com','h.hafez@nouracademy.com'];
            Mail::send('emails.admin_notification', ['candidate' => $candidate  ], function ($m) use ($candidate,$emails) {
            $m->from('platform@nouracademy.com', 'Nour Academy');
            $m->to($emails)->subject(' Volunteers | Candidate finished test');
            });
        }
        if($grade >= $pass_degree){
            $user->user_phase = "phoneInterview";
            $user->user_status = "Pending";
            $message = "<p>Congratulations you have passed the Islamic knowledge test. </p><p>In the next phase a member of our team will contact you via phone to conduct a quick interview to assess your level of English fluency. </p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
            $emails = ['mtarek@arabiclocalizer.com', 'm.abdelgawad@nouracademy.com','h.hafez@nouracademy.com'];
            Mail::send('emails.admin_notification', ['candidate' => $candidate  ], function ($m) use ($candidate,$emails) {
            $m->from('platform@nouracademy.com', 'Nour Academy');
            $m->to($emails)->subject(' Volunteers | Candidate finished test');
            });
        }
		$user->save();


		$data = [];
        $data['user_id'] = $this->logged_user->id;
        $data['partialView'] = 'user_test.test_finished';
        $data['message'] = $message;
        return view('user_test.base', $data);
    }
}
