<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

class userController extends Controller
{
    public function index(){
    	if(Auth::user()){
	    	if (Auth::user()->user_type == "Admin"){
	    		return redirect('/admin/quizzes');
	    	}else{
	    		return redirect('/admin/current_status');
	    	}
	    }else{
	    	return redirect('/login');
	    }
    }

    public function currentStatus(){
    	if(Auth::user()){
	    	if (Auth::user()->user_type == "Volunteer"){
	    		$user = Auth::user();
	    		$data = [];
		        $data['partialView'] = 'status.status';
		        if($user->user_status == "Pending"){

		       	 if($user->user_phase == "testing"){
		       	 	$data['message'] = "<p>Thank you for applying as a volunteer teacher at Nour Academy, may Allah reward you.</p><p>In order to make sure that we are providing a satisfactory level of education for our students we conduct a series of screening phases to qualify our volunteer teachers.</p><p>The first phase in screening is the Islamic knowledge test, which is a simple multiple choice test that evaluates whether you have the basic information to qualify you to teach Islamic princliples.</p><p>To start the test <a href='/admin/user_test' >click here</a>.</p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
		       	 }
		       	 if($user->user_phase == "testEvaluation"){
		       	 	$data['message'] = "<p>You have currently completed the Islamic knowledge test. Someone from our team will review your test and you will receive an email with the test results.   </p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
		       	 }
		       	 if($user->user_phase == "phoneInterview"){
		       	 	$data['message'] = "<p>Congratulations you have passed the Islamic knowledge test. </p><p>In the next phase a member of our team will contact you via phone to conduct a quick interview to assess your level of English fluency. </p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
		       	 }
		       	 if($user->user_phase == "pilotTest"){
		       	 	if($user->pilot_ready == 0){
		       	 		$data['message'] = "<p>Congratulations you have passed the phone interview phase. Now you are just one step away from becoming a volunteer teacher at Nour Academy. </p><p>In this phase you have access to our training material and course content which you can view by clicking the respective tabs on the left.  </p><p>The training material covers how to use our course material and our virtual classrooms. Please review the training material thoroughly. This should take no longer than a couple of hours. Afterwards take a quick look at the course material tab for examples ( <span style='font-weight:bold;color:red; text-tranform:uppercase;'>We have included all our course content for reference only, you only need to review a few samples</span> )</p><p>When you feel you are comfortable understanding how to use our course material and virtual classroom please click on the tab [Ready for pilot test] on the left bar and click yes. After which a member of our team will get in touch with you to determine a date and time when we can conduct a safe pilot test where you will act as a teacher and someone from our team will act as a student. </p><p>If you have any questions or face any difficulties or technical problems do not hesitate to contact us at training.questions@nouracademy.com.  </p><p>This is the last phase in screening to make sure that you are ready to start providing lessons to non-Arabic speaking students.</p><p>Thank you for your patience and may Allah reward your efforts.</p>" ;
		       	 	}
		       	 	if($user->pilot_ready == 1){
		       	 		$data['message'] = "<p>A member of our team will contact you shortly to set a date and time for the pilot test. </p> " ;
		       	 	}
		       	 	
		       	 }
		    	}
		    	if($user->user_status == "Rejected"){
		       	 $data['message'] = "Unfortunately you were unable to become an Islamic Practices instructor at Nour Academy since you did not pass the ";
		       	 if($user->user_phase == "testing" || $user->user_phase == "testEvaluation"){
		       	 	$data['message'] .= "Islamic knowledge screening phase" ;
		       	 }
		       	 if($user->user_phase == "phoneInterview"){
		       	 	$data['message'] .= "phone interview screening phase" ;
		       	 }
		       	 if($user->user_phase == "pilotTest"){
		       	 	$data['message'] .= "pilot testing phase" ;
		       	 }
		       	 
		    	}
		        if($user->user_status == "Approved"){
		       	 $data['message'] = "<p>Congratulations! you are currently a volunteer Islamic practices teacher at Nour Academy. May Allah bless your efforts and intentions.</p><p>Our registration team should contact you to check for your availability whenever a new student group is ready to take a course.</p>";
		    	}
		        return view('status.base', $data);
	    	}
	    }
    }
}
