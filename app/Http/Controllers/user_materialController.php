<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Material;
use File;
use ZipArchive;
use RecursiveIteratorIterator;
use FilesystemIterator;
use Storage;
use Response;

class user_materialController extends Controller
{
    public function index($material_type,$course) {
        $data = [];
        $data['partialView'] = 'user_material.list';
        if($course ==0){
            $data['materials'] = Material::where('material_type',$material_type)->orderBy('material_order', 'asc')->get();
        }else{
            $data['materials'] = Material::where('material_type',$material_type)->where('course_id',$course)->orderBy('material_order', 'asc')->get();

        }
        $data['material_type'] = $material_type;
        $data['course'] = $course;
        return view('user_material.base', $data);
    }


    public function view($id) {
        $material = Material::findOrFail($id);

        $path = '/uploads/material/material_file/'.$id.'/index.html';


	    
	    

        $data = [];

        $data['partialView'] = 'user_material.view';
        $data['material'] = $material;
        $data['material_type'] = "";
        $data['course'] = "";
        $data['path'] = $path;
        return view('user_material.base', $data);
    }
    
    public function materialFile($id) {
        $material = Material::findOrFail($id);
        if ($material->materialFile()) {
            return response()->download($material->materialFile()->path, $material->materialFile()->name);
        }
        abort(404);
    }
    

    
}
