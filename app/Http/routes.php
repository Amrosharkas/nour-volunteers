<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'userController@index')->name('index');
Route::get('/admin/current_status', 'userController@currentStatus')->name('currentStatus');

Route::group(['middleware' => ['auth'], 'prefix' => "admin/quizzes", 'as' => "admin.quizzes."], function () {
    Route::get('/', function () {
        return redirect('/admin/quizzes/1/edit');
    });
    Route::get('{id}/edit', 'quizController@edit')->name('edit');
    Route::post('question_add/{quiz_id}', 'quizController@addQuestion')->name('question_add');
    Route::post('answer_add/{quiz_id}', 'quizController@addAnswer')->name('answer_add');
    Route::get('answers/{question_id}', 'quizController@answers')->name('answers');
    Route::delete('{id}/delete', 'quizController@delete')->name('delete');
    Route::post('quizzes/{id}/init_question', 'quizController@initQuestion')->name('init_question');
    Route::post('{id}/upload_question_file', 'quizController@uploadQuestionFile')->name('upload_question_file');
    Route::post('{id}/upload_answer_file', 'quizController@uploadAnswerFile')->name('upload_answer_file');
    Route::get('{id}/question_file', 'quizController@questionFile')->name('question_file');
    Route::get('{id}/answer_file', 'quizController@answerFile')->name('answer_file');
    Route::delete('{id}/delete_question_file', 'quizController@deleteQuestionFile')->name('delete_question_file');
    Route::delete('{id}/delete_answer_file', 'quizController@deleteAnswerFile')->name('delete_answer_file');
    Route::delete('{id}/delete_course_image', 'quizController@deleteCourseImage')->name('delete_course_image');
    Route::delete('{id}/delete_promo', 'quizController@deletePromo')->name('delete_promo');
    Route::patch('{id}/update', 'quizController@update')->name('update');
    Route::patch('{id}/update_answers', 'quizController@updateAnswers')->name('update_answers');
});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin/users', 'as' => 'admin.users.'], function () {
    Route::get('/{status}', 'UserController@index')->name('index');

    Route::post('init', 'UserController@init')->name('init');
    Route::get('{id}/edit', 'UserController@edit')->name('edit');
    Route::delete('{id}/delete', 'UserController@delete')->name('delete');
    Route::post('{id}/recommission', 'UserController@recommission')->name('recommission');
    Route::get('create', 'UserController@create')->name('create');
    Route::delete('delete_multiple', 'UserController@deleteMultiple')->name('delete_multiple');
    Route::post('store', 'UserController@store')->name('store');
    Route::patch('{id}/update', 'UserController@update')->name('update');
});

Route::group(['middleware' => ['auth'], 'prefix' => 'admin/user_test', 'as' => 'admin.user_test.'], function () {
    Route::get('/', 'volunteersTestController@index')->name('index');

    Route::get('startTest/{user_id}', 'volunteersTestController@startTest')->name('start_test');
    Route::post('answerQuestion', 'volunteersTestController@answerQuestion')->name('answer_question');
    Route::get('endTest', 'volunteersTestController@endTest')->name('end_test');
    
});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/material", 'as' => "admin.material."], function () {
    Route::get('/', 'materialController@index')->name('index');
    Route::post('init', 'materialController@init')->name('init');
    Route::get('{id}/edit', 'materialController@edit')->name('edit');
    Route::delete('{id}/delete', 'materialController@delete')->name('delete');
    Route::post('{id}/upload_material_file', 'materialController@uploadMaterialFile')->name('upload_material_file');
    Route::get('{id}/material_file', 'materialController@materialFile')->name('material_file');
    Route::delete('{id}/delete_material_file', 'materialController@deleteMaterialFile')->name('delete_material_file');
    Route::patch('{id}/update', 'materialController@update')->name('update');
    Route::get('deleteCourse/{id}', 'materialController@deleteCourse')->name('deleteCourse');
});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/user_material", 'as' => "admin.user_material."], function () {
    Route::get('/getMaterials/{material_type}/{course}', 'user_materialController@index')->name('index');
    Route::get('{id}/view', 'user_materialController@view')->name('view');
    Route::get('{id}/material_file', 'user_materialController@materialFile')->name('material_file');
});

Route::group(['middleware' => ['auth'], 'prefix' => "admin/candidates", 'as' => "admin.candidates."], function () {
    Route::get('/{user_phase}/{user_status}', 'candidateController@index')->name('index');
    Route::post('{id}/pass', 'candidateController@pass')->name('pass');
    Route::post('{id}/fail', 'candidateController@fail')->name('fail');
    Route::get('{id}/edit/edit', 'candidateController@edit')->name('edit');
    Route::get('pilotReady', 'candidateController@pilotReady')->name('pilot_ready');
    Route::get('add', 'candidateController@add')->name('add');
    Route::get('{id}/test_details/test_details', 'candidateController@testDetails')->name('test_details');
    Route::patch('save', 'candidateController@save')->name('save');
    Route::patch('saveEdit', 'candidateController@saveEdit')->name('saveEdit');

});

Route::auth();

Route::get('/home', 'HomeController@index');
