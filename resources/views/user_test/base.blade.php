<?php $i = "user_test"; $j = "user_test"; ?>
@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.css')}}"/>
<style>
/* Customization Style of SyoTimer */
        .timer{
            text-align: center;

            margin: 30px auto 0;
            padding: 0 0 10px;

            border-bottom: 2px solid #80a3ca;
        }
        .timer .table-cell{
            display: inline-block;
            margin: 0 5px;

            width: 79px;
            background: url(../demos/images/timer.png) no-repeat 0 0;
        }
        .timer .table-cell .tab-val{
            font-size: 35px;
            color: #80a3ca;

            height: 81px;
            line-height: 81px;

            margin: 0 0 5px;
        }
        .timer .table-cell .tab-unit{
            font-family: Arial, serif;
            font-size: 12px;
            text-transform: uppercase;
        }

        #simple_timer.timer .table-cell.day,
        #periodic_timer_days.timer .table-cell.hour{
            width: 120px;
            background-image: url(../demos/images/timer_long.png);
        }
		.day,.second{
			display:none !important;
		}
		.timer {
    font-family: "Segment7Standard";
    font-size: 30px;
    display: inline-block;
    vertical-align: top;
	margin:0 !important;
	padding:0 !important;
}
.clock {
    margin-top: 0  !important ;
}
.days {
  float: left;
  margin-right: 4px;
}
.hours {
  float: left;
}
.minutes {
  float: left;
}
.seconds {
  float: left;
}
.clearDiv {
  clear: both;
}


</style>
@stop

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script>
@stop

@section('page_js')
<script>
var pageAttributes = {
    indexUrl: "",
}

</script>
<script type="text/javascript" src="{{asset('assets/ajaxForms.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script src="{{asset('js/countdown/jquery.simple.timer.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/user_test.js')}}"></script>
@endSection

@section('add_inits')

@stop

@section('title')
Quizzes
@stop

@section('page_title')
Quizzes
@stop

@section('page_title_small')

@stop

@section('content')

@include($partialView)
@stop

