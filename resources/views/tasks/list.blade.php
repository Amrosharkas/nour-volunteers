<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Tasks
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
    @if(Auth::user()->user_type == "User")
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn green" id="add_new_course" data-action="{{route('admin.tasks.init')}}">
                            Add New <i class="fa fa-plus"></i>
                        </button>                        
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
    @endif
        <table class="table table-striped table-bordered table-hover table-dt" id="table-dt">
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">
                        User
                    </th>
                    <th valign="middle">Email Subject</th>
                    <th valign="middle">Total Duration</th>
                    <th valign="middle">Status</th>
                   @if(Auth::user()->user_type == "Allocator") <th valign="middle">Approval Status</th>@endif
                   @if(Auth::user()->user_type == "User")<th valign="middle">Approval Status</th>@endif
                    <th valign="middle">
                        Started on
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($tasks as $task)
                <tr class="odd gradeX <?php if($task->notification == 1 && Auth::user()->user_type == "User"){?>success<?php ;}?>" id="data-row-{{$task->id}}">
                    <td valign="middle">
                        <a href="/admin/tasks/{{$task->id}}/details" class="popup">{{$task->name}}</a>
                    </td>
                    <td valign="middle"><?php if(isset($task->getUser->name)){?>{{$task->getUser->name}}<?php ;}?>
                        
                    </td>
                    <td valign="middle">{{$task->email_subject}}</td>
                    <td valign="middle"><?php
					$hours = floor($task->duration/(60*60));
					$mins = floor((($task->duration - ($hours*60*60) )/60));
					echo $hours.":".$mins;
					 ?></td>
                    <td valign="middle">
                    @if(Auth::user()->user_type == "User")
                    @if($task->status != 'Finished')
                    <div class="buttons">
                    <button class="btn blue running_status show_form" data-status="{{$task->status}}" @if($task->status=="Paused")data-type="resume"@endif @if($task->status=="Running")data-type="pause"@endif>
                        @if($task->status=="Paused")<i class="fa fa-pause" aria-hidden="true"></i> @endif
                        @if($task->status=="Running")<i class="fa fa-play" aria-hidden="true"></i> @endif
                        {{$task->status}}
                    </button>
                    @if($task->status=="Running")<button class="btn red show_form" data-type="end"><i class="fa fa-stop" aria-hidden="true"></i> End</button>@endif
                    </div>
                    <div class="timepickerDiv"  style="display:none;">
                    <form class="ajax_form1" action="/admin/tasks/updateTaskStatus"  method="post">
                    <input placeholder="Insert time" type="text" class="form-control timepicker pull-left" name="time" style="width:150px;"  />
                    <input type="hidden" name="task_id" value="{{$task->id}}" />
                    <input type="hidden" name="status" value="{{$task->status}}" />
                    <input type="hidden" name="log_type" value="" class="log_type" />
                    <input type="hidden" name="day" value="<?php echo date("d",strtotime($task->last_start_time));?>"  />
                    
                    <button class="btn blue pull-left submit_log" type="submit"><i class="fa fa-thumbs-up" aria-hidden="true"></i></button>
					<a href="#" class="btn red cancel_update pull-left" ><i class="fa fa-remove"></i> </a>
                    
                    </form>
                    </div>
 					@endif
                    @if($task->status == 'Finished')
                    Finished
                    
                    @endif
                    
                    @endif
                    @if(Auth::user()->user_type == "Allocator")
                    	{{$task->status}}
                    @endif
                    </td>
                   @if(Auth::user()->user_type == "Allocator") <td align="center" valign="middle">@if($task->approval_status == "None"){{$task->approval_status}}@endif 
                   @if($task->approval_status != "None")
                   <select name="task-status" data-task_id="{{$task->id}}" class="approval_select">
                   	<option disabled="disabled" value="Pending" <?php if($task->approval_status == "Pending"){?> selected="selected" <?php ;}?>>Pending</option>
                   	
                   	<option value="Rejected" <?php if($task->approval_status == "Rejected"){?> selected="selected" <?php ;}?>>Approved</option>
                   </select>
                   @endif
                   @if($task->comment!="Ok" && $task->approval_status == "Rejected") <a href="#" class="popovers red" data-container="body" data-trigger="hover" data-placement="top" data-content="{{$task->comment}}" data-original-title="Comment">Comment</a> @endif
                   
                   </td>@endif
                   @if(Auth::user()->user_type == "User")<td align="center" valign="middle">
                   @if($task->approval_status!="Rejected")
                   {{$task->approval_status}} 
                   @endif
                   @if($task->comment!="Ok" && $task->approval_status == "Rejected") <a href="#" class="popovers red" data-container="body" data-trigger="hover" data-placement="top" data-content="{{$task->comment}}" data-original-title="Comment">Approved with a comment</a> @endif</td>@endif
                    <td valign="middle">
                        {{ Carbon\Carbon::parse($task->start_time)->format('D d, M Y H:i') }}
                        
                    </td>
                    <td valign="middle">
                    	@if($task->status != "Finished" || Auth::user()->user_type == "Allocator")
                        <a href="{{route('admin.tasks.edit',['id'=>$task->id])}}" class="btn green pjax-link" ><i class="fa fa-edit"></i> Edit</a> 
                        @endif
                    	@if(Auth::user()->user_type == "Allocator")
                        <a href="{{route('admin.tasks.delete',['id'=>$task->id])}}" class="btn red remove-course" ><i class="fa fa-remove"></i> Delete</a> 
                        @endif
                        @if($task->status != "Finished" || Auth::user()->user_type == "Allocator")
                        <a href="{{route('admin.tasks.logs',['id'=>$task->id])}}" class="btn blue popup" ><i class="fa fa-clock-o"></i> logs</a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>