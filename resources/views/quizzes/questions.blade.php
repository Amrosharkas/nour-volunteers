<ol class="dd-list">
    @foreach($questions as $question)
           <li class="dd-item question shadow"  data-content="{{$question['content']}}" data-id = "{{$question['id']}}" data-weight = "{{$question['weight']}}">
    
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows" aria-hidden="true"></i>

 </div>
        <div class="question_body" >
        <a class="btn btn-primary answers_links popup" href="{{route('admin.quizzes.answers',['question_id'=>$question['id']])}}"  ><i class="fa fa-edit"></i> Answers </a>
        <a class="btn red remove_question" ><i class="fa fa-remove"></i></a>
            <input type="hidden" name="id">
            <div class="row">
            	<div class="col-md-9">
               		<div class="form-group">
                    <label>Question</label>
                    <input type="text" class="form-control" name="objective-content" value="{{$question['content']}}" placeholder="Type the question here" />
                    
                </div>
                </div>
                <div class="col-md-3">
                	<div class="form-group">
                    <label>Weight</label>
                    <input type="text" class="form-control" name="question-weight" value="{{$question['weight']}}" placeholder="Weight" />
                </div>
                </div>
            </div>
            <div class="form-group">
                    <label >Attach File</label>
                    <input type="file" class="hidden fileinput"
                           data-action="{{route('admin.quizzes.upload_question_file',['id'=>$question['id']])}}" 
                           >
                    <div class="row">
                        <div class="col-md-8">
                            <span class="filenameplaceholder">
                                @if($question['file'])
                                <a class="btn btn-primary filename" href="{{route('admin.quizzes.question_file',['id'=>$question['id']])}}" >
                                  <i class="fa fa-edit"></i>  Download File
                                </a>
                                @else
                                No file selected
                                @endif
                            </span>
                            <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                <span>0%</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary filepicker pull-right {{$question['file']?'hidden':''}}">
                                Select File
                            </button>
                            <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                            <button class="btn btn-danger pull-right removefile {{$question['file']?'':'hidden'}}" 
                                    data-action="{{route('admin.quizzes.delete_question_file',['id'=>$question['id']])}}"
                                    >
                                Detach file
                            </button>
                        </div>
                        
                    </div>
                </div>
        </div>
        <div class="dd-handle handle-hidden" ></div>
        
    </li>                     
    
    
    @endforeach
</ol>

