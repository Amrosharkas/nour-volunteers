<ol class="dd-list">
    @foreach($questions as $question)
    <li class="dd-item question"  data-content="{{$question['content']}}" data-id = "{{$question['id']}}">
    
        <div class="dd-handle objective_drag" ><i class="fa fa-arrows-alt" aria-hidden="true"></i>
 </div>
        <div >
            <input type="hidden" name="id">
            <div class="form-group">
                <input type="text" class="form-control" name="objective-content" value="{{$question['content']}}" placeholder="Type the question here" />
                <button class="btn btn-default btn-sm remove_question" type="button"><i class="fa fa-remove"></i> Remove</button>
            </div>
            <div class="form-group">
                    <label >Attach File</label>
                    <input type="file" class="hidden fileinput"
                           data-action="{{route('admin.quizzes.upload_question_file',['id'=>$question['id']])}}" 
                           >
                    <div class="row">
                        <div class="col-md-8">
                            <span class="filenameplaceholder">
                                @if($question['file'])
                                <a href="{{route('admin.quizzes.question_file',['id'=>$question['id']])}}" class="filename">
                                    {{$question['file']}}
                                </a>
                                @else
                                No file selected
                                @endif
                            </span>
                            <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                <span>0%</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary filepicker pull-right {{$question['file']?'hidden':''}}">
                                Select File
                            </button>
                            <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                            <button class="btn btn-danger pull-right removefile {{$question['file']?'':'hidden'}}" 
                                    data-action="{{route('admin.quizzes.delete_question_file',['id'=>$question['id']])}}"
                                    >
                                Remove
                            </button>
                        </div>
                        <a href="{{route('admin.quizzes.answers',['question_id'=>$question['id']])}}" class="popup" > Answers </a>
                    </div>
                </div>
        </div>
        <div class="dd-handle handle-hidden" ></div>
        
    </li>
    @endforeach
</ol>

