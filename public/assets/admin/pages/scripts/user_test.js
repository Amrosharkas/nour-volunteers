$(document).on('ready pjax:success', function () {
    $.getScript("/assets/global/scripts/app.js");
    if ($(".timer").length) {

        
        //alert(year + "-" + month + "-" + day + " " + hour + ":" + mintue);
        // prepare the form when the DOM is ready 
        $(function () {
                $('.timer').startTimer({
                    onComplete: function () {
                        pjaxPage('/admin/user_test/endTest');
                    }
                });
            })
            // timer script -----------------------------------------
    }
    // prepare the form when the DOM is ready 

    $(document).ready(function () {
        var options = {
            target: '', // target element(s) to be updated with server response 
            beforeSubmit: showRequest, // pre-submit callback 
            success: showResponse // post-submit callback 

            // other available options: 
            //url:       url         // override for form's 'action' attribute 
            //type:      type        // 'get' or 'post', override for form's 'method' attribute 
            //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
            //clearForm: true        // clear all form fields after successful submit 
            //resetForm: true        // reset the form after successful submit 

            // $.ajax options can be used here too, for example: 
            //timeout:   3000 
        };

        // bind form using 'ajaxForm' 
        $('.ajax_form').ajaxForm(options);
    });

    // pre-submit callback 
    function showRequest(formData, jqForm, options) {
        $('.submit_answer').hide();
        App.blockUI({
            boxed: true
        });
        
       
        selectedValuex = formData.length; 
        
        


        if (selectedValuex <= 2 ) {
            var $toast = toastr["error"]("Error", "Please select an answer");
            App.unblockUI();
            $('.submit_answer').show();
            return false;
            
        }
        return true;
       


        
    }

    // post-submit callback 
    function showResponse(responseText, statusText, xhr, $form) {

        pjaxPage('/admin/user_test/');
        App.unblockUI();

    }
});
