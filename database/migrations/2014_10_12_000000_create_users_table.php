<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');
            $table->string('user_type')->nullable();
            $table->string('quiz_status')->default('NoQuiz'); // NoQuiz, Started, Finished
            $table->string('user_phase')->default('testing'); // testing,testEvaluation, phoneInterview, pilotTest,PassedAll
            $table->string('user_status')->default('Pending');// Pending, Approved, Rejected
            $table->integer('pilot_ready')->default(0);
            $table->integer('quiz_finished')->default(0);
            $table->datetime('quiz_start_time')->nullable();
            $table->datetime('quiz_end_time')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->float('quiz')->default(0);
            $table->float('accent')->default(0);
            $table->float('grammar')->default(0);
            $table->float('vocab')->default(0);
            $table->float('fluency')->default(0);
            $table->float('pilot')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
        Schema::drop('user_grades');
    }
}
