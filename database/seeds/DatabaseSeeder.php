<?php

use Illuminate\Database\Seeder;
use App\User;
use App\quiz;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Admin
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@nouracademy.com';
        $user->password = bcrypt('123456');
        $user->user_type = 'Admin';
        $user->save();
        // Volunteer 1
        $user = new User();
        $user->name = 'Volunteer 1';
        $user->email = 'Volunteer1@nouracademy.com';
        $user->password = bcrypt('123456');
        $user->user_type = 'Volunteer';
        $user->save();
        // Volunteer 2
        $user = new User();
        $user->name = 'Volunteer 2';
        $user->email = 'Volunteer2@nouracademy.com';
        $user->password = bcrypt('123456');
        $user->user_type = 'Volunteer';
        $user->save();
        // Default Quiz
        $quiz = new quiz();
        $quiz->title = 'Islamic knowledge test';
        $quiz->weight = 1;
        $quiz->save();
    }
}
